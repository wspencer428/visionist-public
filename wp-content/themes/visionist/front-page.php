<?php
/**
 * front-page.php
 *
 * @package bootstrapped
 */

get_header(); ?>
	<div class="fullscreen-bg">
	    <video loop muted autoplay class="fullscreen-bg__video">
	        <source src="<?php echo get_bloginfo('url'); ?>/media/SiteVid1.mp4" type="video/mp4" />
	    </video>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<h1>FRONT PAGE</h1>
		</div>
	</div>


<?php get_footer(); ?>